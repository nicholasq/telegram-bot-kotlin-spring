package xyz.nixqloud.bot.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import xyz.nixqloud.bot.service.MessageInterpreterService

@RestController("botController")
class BotControllerImpl @Autowired constructor(
        private val messageInterpreterService: MessageInterpreterService
) : BotController {

    private val log = LoggerFactory.getLogger(BotController::class.java)!!

    @PostMapping("\${telegram.bot.webhook-path}")
    override fun update(@RequestBody update: Update): BotResponse {

        log.info("update() - $update")

        try {
            return messageInterpreterService.interpret(update)

        } catch (e: Exception) {

            log.error("update() - $update", e)

            val botResponse = BotResponse()
            botResponse.chatId = update.message?.chat?.id
            botResponse.text = "Sorry but something went wrong"
            return BotResponse()
        }
    }
}