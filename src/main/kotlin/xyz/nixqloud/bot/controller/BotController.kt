package xyz.nixqloud.bot.controller

import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update

interface BotController {
    fun update(update: Update): BotResponse
}