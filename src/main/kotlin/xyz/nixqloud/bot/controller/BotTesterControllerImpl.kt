package xyz.nixqloud.bot.controller

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import xyz.nixqloud.bot.json.Chat
import xyz.nixqloud.bot.json.Message
import xyz.nixqloud.bot.json.Update

@Controller("botTesterController")
class BotTesterControllerImpl @Autowired constructor(
        val botController: BotController
) : BotTesterController {

    private val log = LoggerFactory.getLogger(BotController::class.java)

    @GetMapping("/testbot")
    override fun testBotPage(): String {

        log.info("testBotPage() - got request for tester page")
        return "tester"
    }

    @PostMapping("/testmessage")
    @ResponseBody
    override fun testBotPost(@RequestParam("text", required = true) text: String): String {
        return botController.update(
                Update(
                        updateId = 9999,
                        message = Message(
                                id = 9999,
                                text = text,
                                chat = Chat(id = 9999)
                        )
                )
        ).text!!
    }
}