package xyz.nixqloud.bot.controller

interface BotTesterController {
    fun testBotPage(): String
    fun testBotPost(text: String): String
}