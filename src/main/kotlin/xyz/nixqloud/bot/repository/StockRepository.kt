package xyz.nixqloud.bot.repository

import org.springframework.stereotype.Repository

interface StockRepository {

    fun getStock(stock: String): String
}

@Repository("stockRepository")
class StockRepositoryImpl : StockRepository {

    override fun getStock(stock: String): String = "Work in progress..."

}