package xyz.nixqloud.bot.service

import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.net.HttpURLConnection
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.URL

@Service("httpService")
class HttpServiceImpl : HttpService {

    private val log = LoggerFactory.getLogger(HttpServiceImpl::class.java)!!

    override fun get(url: String): String {
        return get(url, emptyMap())
    }

    override fun get(url: String, headers: Map<String, String>): String {

        var httpConnection: HttpURLConnection? = null

        try {

            httpConnection = URL(url).openConnection() as HttpURLConnection
            headers.forEach(httpConnection::setRequestProperty)
            val bytes = ByteArrayOutputStream()

            if (httpConnection.responseCode != HttpURLConnection.HTTP_OK) {
                log.warn("get() - received reponse message: {} {}", httpConnection.responseCode, httpConnection.responseMessage)
                return ""
            }
            httpConnection.inputStream.use {
                it.copyTo(bytes)
                return String(bytes.toByteArray())
            }

        } catch (e: Exception) {
            log.error("httpGet() - could not download: $url", e)
            return ""
        } finally {
            httpConnection?.disconnect()
        }
    }

    override fun postJson(url: String, json: String): String {

        var httpClient: CloseableHttpClient? = null

        try {

            httpClient = HttpClientBuilder.create().build()
            val httpPost = HttpPost(url)
            httpPost.addHeader("content-type", "application/json")
            httpPost.entity = StringEntity(json)
            val response = httpClient.execute(httpPost)
            val output = ByteArrayOutputStream()

            response.entity.content.use {
                it.copyTo(output)
                return String(output.toByteArray())
            }

        } catch (e: Exception) {
            log.error("httpPostJson() - could not post : $json", e)
            return ""
        } finally {
            httpClient?.close()
        }
    }

}