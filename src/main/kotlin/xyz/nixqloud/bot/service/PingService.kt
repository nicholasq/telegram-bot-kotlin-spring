package xyz.nixqloud.bot.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

@Service("pingService")
open class PingService @Autowired constructor(
        private val context: ApplicationContext
) : BasicBotService {

    companion object {
        const val COMMAND = "/ping"
    }

    override fun handleUpdate(update: Update): BotResponse {

        val startupTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(context.startupDate), ZoneId.systemDefault())
        val message = "Startup time: $startupTime"

        return BotResponse(
                chatId = update.message?.chat?.id,
                text = message
        )
    }
}