package xyz.nixqloud.bot.service

import xyz.nixqloud.bot.json.BotResponse

interface TelegramService {
    fun sendMessage(botResponse: BotResponse): String
}
