package xyz.nixqloud.bot.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import java.io.File
import java.time.LocalDateTime

@Service("commandService")
class CommandServiceImpl : CommandService, BasicBotService {

    companion object {
        const val COMMAND_PYTHON = "/python"
        const val COMMAND_BASH = "/bash"
        const val COMMAND_JAVA = "/java"
        const val COMMAND_JAVASCRIPT = "/javascript"
        const val COMMAND_KOTLIN = "/kotlin"
        val log = LoggerFactory.getLogger(CommandService::class.java)!!
        val spaceOrNewline = Regex("(\\s|\n)")
    }

    val botCodeDirRelPath = "botcode"
    val botCodeDirAbsPath: String by lazy {
        val currentWorkingDir = File(".").absolutePath.let { it.substring(0, it.length - 1) }
        val botcodeDir = File(currentWorkingDir, botCodeDirRelPath)
        if (!botcodeDir.exists()) botcodeDir.mkdir()
        botcodeDir.absolutePath
    }

    override fun handleUpdate(update: Update): BotResponse {

        val text = update.message!!.text!!
        val result = if (!text.contains(spaceOrNewline)) {
            "You need a space or new line after the command"
        } else {
            parseCommand(update)
        }
        return BotResponse(text = if (result.length > 1000) result.substring(0, 1000) else result)
    }

    fun parseCommand(update: Update): String {

        val text = update.message!!.text!!
        val program = processText(text)
        val code = text.substring("/$program".length).trim()
        val message = update.message!!
        val chatInfo = ChatInfo(chatId = message.chat!!.id!!.toString(), userId = message.from!!.id!!.toString())

        return when (program) {
            COMMAND_PYTHON -> executePython(code, chatInfo)
            COMMAND_BASH -> executeBash(code, chatInfo)
            COMMAND_JAVASCRIPT -> executeJavascript(code, chatInfo)
            COMMAND_KOTLIN -> "$program isn't supported yet. Coming soon!"
            COMMAND_JAVA -> "$program isn't supported yet. Coming soon!"
            else -> "Sorry. No support for $program"
        }
    }

    private fun processText(text: String): String {

        var token: Int = text.length

        for (i in 0 until text.length) {
            val ch = text[i]
            if (ch == ' ' || ch == '\n') {
                token = i
                break
            }
        }

        return text.substring(0, token)
    }

    private fun makeProgram(code: String, chatInfo: ChatInfo): String {

        val (chatId, userId) = chatInfo
        val fileName = makeFileName(chatId, userId, "py")
        val codeFile = CodeFile(code, fileName)
        writeCodeFile(codeFile)
        return fileName
    }

    private fun executePython(code: String, chatInfo: ChatInfo): String {

        val fileName = makeProgram(code, chatInfo)
        val programArgs = mutableListOf("python", "python")
        return execute(programArgs, fileName)
    }

    private fun executeBash(code: String, chatInfo: ChatInfo): String {

        val fileName = makeProgram(code, chatInfo)
        val programArgs = mutableListOf("bash", "bash")
        return execute(programArgs, fileName)
    }

    private fun executeJavascript(code: String, chatInfo: ChatInfo): String {

        val fileName = makeProgram(code, chatInfo)
        val programArgs = mutableListOf("node", "node")
        return execute(programArgs, fileName)
    }

    private fun writeCodeFile(codeFile: CodeFile): String {

        val (code, fileName) = codeFile
        val tempDir = File(botCodeDirAbsPath)

        if (!tempDir.isDirectory) tempDir.mkdir()

        val file = File(tempDir, fileName)
        file.createNewFile()

        file.outputStream().use {
            it.write(code.toByteArray())
        }

        return file.name
    }

    override fun execute(args: List<String>, fileName: String): String {

        val volumeArg = "botcode:/app/$botCodeDirRelPath/"
        val programArgs = mutableListOf("docker", "run", "--rm", "-v", volumeArg)
        programArgs.addAll(args)
        programArgs.add("/app/$botCodeDirRelPath/$fileName")
        println(programArgs)

        val processBuilder = ProcessBuilder(programArgs)

        processBuilder.start().run {
            try {
                val err = String(this.errorStream.readBytes())
                val out = String(this.inputStream.readBytes())
                return if (err.isNotEmpty()) err
                else out
            } finally {
                File("$botCodeDirRelPath/$fileName").delete()
            }
        }
    }

    private fun makeFileName(chatId: String, userId: String, ext: String): String {
        return "temp_${chatId}_${userId}_${LocalDateTime.now().toString().replace(':', '_')}.$ext"
    }
}

private data class CodeFile(val code: String, val fileName: String)
private data class ChatInfo(val chatId: String, val userId: String)
