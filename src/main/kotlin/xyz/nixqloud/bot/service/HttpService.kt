package xyz.nixqloud.bot.service

interface HttpService {
    fun get(url: String): String
    fun get(url: String, headers: Map<String, String>): String
    fun postJson(url: String, json: String): String
}