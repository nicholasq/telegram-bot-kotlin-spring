package xyz.nixqloud.bot.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import java.net.URLEncoder

@Service("urbanDictionaryService")
class UrbanDictionaryService @Autowired constructor(
        val httpService: HttpService
) : BasicBotService {

    companion object {
        const val SERVICE_NAME = "urbanDictionaryService"
        const val COMMAND = "/urband"
        private const val urbanDictionaryApiUrl = "http://api.urbandictionary.com/v0/define?term="
        private val jsonMapper = ObjectMapper()
    }

    override fun handleUpdate(update: Update): BotResponse {

        val text = update.message!!.text!!.trim()

        if (text.length < COMMAND.length + 2) {
            return BotResponse(text = "You need to specify a word. ex: $COMMAND slippin")
        }

        val word = text.substring(COMMAND.length + 1).trim()
        val query = urbanDictionaryApiUrl + URLEncoder.encode(word, "utf-8")

        val jsonStr = httpService.get(query)

        if (jsonStr.isEmpty()) return BotResponse(text = "Couldn't find anything for '$word'")

        val jsonNode = jsonMapper.readTree(jsonStr)

        try {

            val definitions = jsonNode.get("list")
            val firstDefinition = definitions.get(0)
            val definition = firstDefinition.get("definition").asText()
            val word = firstDefinition.get("word").asText()
            val author = firstDefinition.get("author").asText()
            val example = firstDefinition.get("example").asText()
            val permalink = firstDefinition.get("permalink").asText()

            val response = """
                |**Word**
                |$word
                |
                |**Definition**
                |$definition
                |
                |**Example**
                |$example
                |
                |Author: $author
                |
                |[view here]($permalink)
            """.trimMargin()
            return BotResponse(text = response, parseMode = "Markdown")

        } catch (e: Exception) {
            return BotResponse(text = "Couldn't find anything for '$word'")
        }
    }
}