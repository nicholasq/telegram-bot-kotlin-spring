package xyz.nixqloud.bot.service

import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update

interface BasicBotService {

    fun handleUpdate(update: Update): BotResponse
}