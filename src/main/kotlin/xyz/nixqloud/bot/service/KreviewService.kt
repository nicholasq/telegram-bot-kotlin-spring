package xyz.nixqloud.bot.service

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.runBlocking
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import java.net.URL

@Service("kreviewService")
class KreviewService : MessageInterceptorService {

    companion object {
        const val SERVICE_NAME = "kreviewService"
    }

    val log = LoggerFactory.getLogger(KreviewService::class.java)!!

    override fun handleUpdate(update: Update): BotResponse = runBlocking {

        val msg = update.message!!.text!!.trim()

        if (!msg.contains("https://www.amazon.com/")) return@runBlocking BotResponse()

        val link = msg.substring(msg.indexOf("https://www.amazon.com")).split(" ")[0]

        log.info("handleUpdate() - received amazon link: $link")

        val fakespotAsync = async(CommonPool) {

            val html = fetchFakespotHtml(link).await()
            val fakespot = parseFakespotHtmlWithAsync(html).await()

            return@async fakespot
        }

        val fakespot = fakespotAsync.await()

        log.info("handleUpdate() - parsed fakespot: $fakespot")

        if (fakespot.grade.isEmpty() && fakespot.percentageReviews.isEmpty()) return@runBlocking BotResponse()

        val text = """
            |**Product**
            |${fakespot.productName}
            |
            |**Grade**
            |${fakespot.grade}
            |
            |**Total Reviews**
            |${fakespot.totalReviews}
            |
            |**Real Reviews**
            |${fakespot.percentageReviews}
            |
            |**TrustWerty Rating**
            |${fakespot.trustWertyRating}/5 stars
        """.trimMargin()
        return@runBlocking BotResponse(text = text, parseMode = "Markdown")
    }
}

private fun fetchHtml(url: String) = async(CommonPool) {
    try {
        URL(url).readText()
    } catch (e: Exception) {
        ""
    }
}

private fun fetchFakespotHtml(amazonUrl: String) = fetchHtml("https://fakespot.com/analyze?url=$amazonUrl")

private fun parseFakespotHtmlWithAsync(html: String) = async(CommonPool) {

    if (html.isEmpty() || !html.contains("<html>") || !html.contains("<body>")) return@async Fakespot()

    val body = findBody(html)
    val productDataSection = async(CommonPool) { findProductDataSection(body) }
    val gradeBox = findGradebox(body)

    if (gradeBox.childNodeSize() == 0) return@async Fakespot()

    val productName = async(CommonPool) { findProductName(productDataSection.await()) }
    val grade = async(CommonPool) { findGrade(productDataSection.await()) }
    val trustWerty = async(CommonPool) { findTrustWerty(productDataSection.await()) }
    val totalReviews = async(CommonPool) { findTotalReviews(productDataSection.await()) }
    val percentageReviews = async(CommonPool) { findPercentageReviews(gradeBox) }
    val companyName = async(CommonPool) { findCompanyName(productDataSection.await()) }

    return@async Fakespot(
            productName = productName.await().trim(),
            grade = grade.await().trim(),
            trustWertyRating = trustWerty.await().trim(),
            totalReviews = totalReviews.await().trim(),
            percentageReviews = percentageReviews.await().trim(),
            company = companyName.await().trim())

}

private fun findBody(html: String): Element {
    return Jsoup.parse(html).body()
}

private fun hasProductBeenAnalyzed(body: Element): Boolean {
    val analyzedMessage = body.select(".loading-text")?.first()?.html() ?: ""
    return !analyzedMessage.contains("This product looks new to us, please wait")
}

private fun findPercentageReviews(gradeBox: Element): String {
    return gradeBox.select("b[class^=font-grade]")?.first()?.html() ?: ""
}

private fun findGradebox(jsoup: Element): Element {

    val gradeBox = jsoup.select(".grade-box.badge")

    if (gradeBox.size == 0) return Element(HTML_STUB)
    else return gradeBox.first()
}

private fun findTotalReviews(productDataSection: Element): String {
    return productDataSection.select(".col-custom.report-button.review-button p")
            ?.html() ?: ""
}

private fun findTrustWerty(productDataSection: Element): String {
    return productDataSection.select("div[id^=star-rating]")
            ?.attr("rating") ?: ""
}

private fun findGrade(productDataSection: Element): String {
    return productDataSection.select(".col-custom.comp-grade.report-button p")
            ?.html() ?: ""
}

private fun findProductName(productDataSection: Element): String {
    return productDataSection.select(".product-link.link-highlight")
            ?.html() ?: "Unknown product name"
}

private fun findProductImage(productDataSection: Element): String {
    return productDataSection.select(".product-image")?.attr("src") ?: ""
}

private fun findCompanyName(productDataSection: Element): String {
    return productDataSection.select(".sub-head.company-label > .link-hightlight")
            ?.html() ?: "Unknown company"
}

private fun findProductDataSection(body: Element): Element {
    val body = body.select(".product-data-section")

    if (body.size == 0) return Element(HTML_STUB)
    else return body.first()
}

private val HTML_STUB = "<html><body></body></html>"

private data class Fakespot(
        val productName: String = "",
        val grade: String = "",
        val trustWertyRating: String = "",
        val totalReviews: String = "",
        val percentageReviews: String = "",
        val company: String = ""
)
