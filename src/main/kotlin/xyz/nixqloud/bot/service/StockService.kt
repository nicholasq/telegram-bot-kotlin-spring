package xyz.nixqloud.bot.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import xyz.nixqloud.bot.repository.StockRepository

@Service("stockService")
open class StockService @Autowired constructor(
        private val stockRepo: StockRepository
) : BasicBotService {


    companion object {
        const val COMMAND = "/stock"
    }

    private val regexWhitespace = Regex("""\s""")
    private val regexAlpha = Regex("""^[a-zA-Z]{1,4}$""")

    override fun handleUpdate(update: Update): BotResponse {

        val args = update.message?.text?.trim()?.split(regexWhitespace) ?: emptyList()
        val result: String

        if (args.size > 1) {

            val symbol = args[1]

            if (regexAlpha.matches(symbol)) {
                val response = stockRepo.getStock(symbol)

                result = if (response.isEmpty())
                    "Stock $response not found"
                else "Something strange happened"

            } else {
                result = "Not a valid stock format. Symbol must be at least one character, but no more than four"
            }


        } else {
            result = "You need to specify a stock symbol. example: AMD"
        }

        return BotResponse(
                chatId = update.message?.chat?.id,
                text = result
        )
    }

}
