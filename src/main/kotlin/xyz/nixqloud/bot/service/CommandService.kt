package xyz.nixqloud.bot.service

interface CommandService {
    fun execute(args: List<String>, fileName: String): String
}