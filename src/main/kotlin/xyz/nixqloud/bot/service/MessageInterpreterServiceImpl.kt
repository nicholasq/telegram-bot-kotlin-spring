package xyz.nixqloud.bot.service

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update

@Service("messageInterpreter")
class MessageInterpreterServiceImpl @Autowired constructor(
        private val context: ApplicationContext,
        private val telegramService: TelegramService
) : MessageInterpreterService {

    private val commandServices = mapOf(
            StockService.COMMAND to "stockService",
            PingService.COMMAND to "pingService",
            ExampleService.COMMAND to "exampleService",
            "/double" to "doubleService",
            CryptoService.COMMAND to "cryptoService",
            RedditService.COMMAND to "redditService",
            ReminderService.COMMAND to "reminderService",
            BotThoughtService.COMMAND to "botThoughtService",
            CommandServiceImpl.COMMAND_PYTHON to "commandService",
            CommandServiceImpl.COMMAND_BASH to "commandService",
            CommandServiceImpl.COMMAND_JAVASCRIPT to "commandService",
            CommandServiceImpl.COMMAND_JAVA to "commandService",
            CommandServiceImpl.COMMAND_KOTLIN to "commandService",
            PythonExtensionService.COMMAND to "pythonExtensionService",
            UrbanDictionaryService.COMMAND to UrbanDictionaryService.SERVICE_NAME
    )

    private val messageSevices = arrayOf(
//            "pythonExtensionService",
            "redditService",
            KreviewService.SERVICE_NAME
    )

    private val log = LoggerFactory.getLogger(MessageInterpreterService::class.java)!!

    // todo: restructure this method. it technically doesn't need to return a bot response.
    // instead find a way to intercept what the telegram mock server has received during tests.
    override fun interpret(update: Update): BotResponse {

        val text = update.message?.text?.trim()
        val args = text?.split(Regex("\\s"))

        if (args != null && args.isNotEmpty()) {

            val chatId = update.message?.chat?.id
            val command = args[0]
            val entry = commandServices.entries.asSequence()
                    .filter { it.key.toLowerCase() == command.toLowerCase() }
                    .firstOrNull()

            if (entry != null) {

                val service = context.getBean(entry.value, BasicBotService::class.java)
                val botResponse = service.handleUpdate(update)

                // If the service didn't explicitly set the chat id, we'll default to what
                // the incoming update has since it's most likely what they want.
                if (botResponse.chatId == null) {
                    botResponse.chatId = chatId
                }

                log.debug("update() - response received: $botResponse")

                telegramService.sendMessage(botResponse)

                return botResponse

            } else if (command == "/commands") {

                val commands = commandServices.keys
                        .joinToString(separator = "\n")
                return BotResponse(chatId = chatId, text = commands).apply {
                    telegramService.sendMessage(this)
                }

            } else {

                messageSevices.asSequence()
                        .map { context.getBean(it, MessageInterceptorService::class.java) }
                        .map { it.handleUpdate(update) }
                        .filter { it.text != null && it.text?.isNotEmpty() ?: false }
                        .onEach { if (it.chatId == null) it.chatId = update.message?.chat?.id }
                        .forEach { telegramService.sendMessage(it) }

                return BotResponse()
            }

        }

        return BotResponse()
    }
}

interface MessageInterpreterService {

    fun interpret(update: Update): BotResponse
}