package xyz.nixqloud.bot.service

import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse

@Service("telegramService")
@Profile("!prod")
class TelegramServiceMockImpl : TelegramService {

    val log = LoggerFactory.getLogger(TelegramService::class.java)

    var botResponse: BotResponse? = null

    override fun sendMessage(botResponse: BotResponse): String {

        log.info("sendMessage() - received: $botResponse")

        this.botResponse = botResponse

        return botResponse.text!!
    }
}