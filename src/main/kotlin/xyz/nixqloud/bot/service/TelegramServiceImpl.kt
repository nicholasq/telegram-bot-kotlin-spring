package xyz.nixqloud.bot.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse

@Profile("prod")
@Service("telegramService")
class TelegramServiceImpl @Autowired constructor(
        val httpService: HttpService,
        val objectMapper: ObjectMapper,
        val env: Environment
) : TelegramService {

    private val log = LoggerFactory.getLogger(TelegramService::class.java)!!

    override fun sendMessage(botResponse: BotResponse): String {

        log.info("sendMessage() - sending to telegram: $botResponse")

        val postResponse = httpService.postJson(
                url = env.getProperty("telegram.bot.url.sendmessage"),
                json = objectMapper.writeValueAsString(botResponse)
        )

        log.info("sendMessage() - post response from telegram: $postResponse")

        return postResponse
    }
}