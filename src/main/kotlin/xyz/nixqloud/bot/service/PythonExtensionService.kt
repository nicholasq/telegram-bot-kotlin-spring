package xyz.nixqloud.bot.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import xyz.nixqloud.bot.json.BotResponse
import xyz.nixqloud.bot.json.Update
import java.io.File

@Service("pythonExtensionService")
class PythonExtensionService @Autowired constructor(
        val commandService: CommandService
) : BasicBotService, MessageInterceptorService {

    init {
        val mainFile = File(absolute_file_path)
        if (!mainFile.exists()) {
            makeFile(header, body, footer)
        }
    }

    companion object {

        const val COMMAND = "/python-extend"
        const val file_name = "python-extend.py"
        val absolute_file_path by lazy {
            "${File(".").absolutePath}/botcode/$file_name"
        }

        val header = """
            |#top
            |import sys
            |args = sys.argv
            |
            |
        """.trimMargin()

        val body = mutableListOf("""
            |#body
            |def main():
            |  print('')
            |  chatId,*text = args
            |
            |
        """.trimMargin())

        var footer = """
            |#footer
            |if __name__ == '__main__':
            |  main()
            |
        """.trimMargin()
    }

    private fun makeFile(header: String, body: MutableList<String>, footer: String) {

        val mainFile = File(absolute_file_path)
        mainFile.outputStream().use { o ->
            o.write(header.toByteArray())
            body.forEach { line -> o.write(line.toByteArray()) }
            o.write(footer.toByteArray())
        }
    }

    // todo: add implementation to read in the python file if it exists.
    private fun readFile(): String = "not implemented"

    override fun handleUpdate(update: Update): BotResponse {

        val text = update.message!!.text!!
        val result: String

        if (text.startsWith(COMMAND)) {
            val code = text.substring(COMMAND.length)
            result = writeCode(code)
        } else {
            result = executeCode(text, update.message!!.chat!!.id!!)
        }

        return BotResponse(text = result)
    }

    fun writeCode(text: String): String {

        if (text.isEmpty() or (text[0] != '\n')) {
            return "You need a new line after the command"
        }

        // Ensure code is indented and add a new line at the end.
        val code = text.split("\n")
                .asSequence()
                .map { "  $it" }
                .joinToString("\n")

        body.add(code)
        // todo: this is where we would read in the file a"extension added"nd assign it to the header/body/footer variables
        // this would allow the file to persist across server reboots
        readFile()
        makeFile(header, body, footer)
        // todo a quick execution of file to check for compilation errors.
        // then roll back previous add if errors.
        return "extension added"
    }

    fun checkCompilation() {

    }

    fun executeCode(text: String, chatId: Int): String {

        println("executeCode called")
        // todo make command service an interface with process command method
        val result = commandService.execute(listOf("python", "python", "/tmp/$file_name", chatId.toString(), "'$text'"), file_name)
        return result
    }
}
