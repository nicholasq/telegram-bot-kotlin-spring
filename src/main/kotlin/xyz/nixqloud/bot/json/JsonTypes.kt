package xyz.nixqloud.bot.json

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.InputStream
import java.util.*
import javax.persistence.*

@JsonInclude(JsonInclude.Include.NON_NULL)
data class User(
        var id: Int? = null,
        var isBot: Int? = null,
        var firstName: Int? = null,
        var lastName: Int? = null,
        var userName: Int? = null,
        var languageCode: Int? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Chat(

        @JsonProperty
        var id: Int? = null,

        @JsonProperty
        var type: String? = null,

        @JsonProperty
        var title: String? = null,

        @JsonProperty
        var username: String? = null,

        @JsonProperty("first_name")
        var firstName: String? = null,

        @JsonProperty("last_name")
        var lastName: String? = null,

        @JsonProperty("all_members_are_administrators")
        var allMembersAreAdministrators: Boolean? = null,

        @JsonProperty
        var photo: Int? = null,

        @JsonProperty
        var description: String? = null,

        @JsonProperty("invite_link")
        var inviteLink: String? = null,

        @JsonProperty("pinned_message")
        var pinnedMessage: Message? = null,

        @JsonProperty("sticker_set_name")
        var stickerSetName: String? = null,

        @JsonProperty("can_set_sticker_set")
        var canSetStickerSet: Boolean? = null
)

@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
data class Message(

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        var id: Long? = null,

        @JsonProperty("message_id")
        var messageId: Long? = null,

        @Transient
        @JsonProperty
        var from: User? = null,

        @JsonProperty
        var date: Long? = null,

        @Transient
        @JsonProperty
        var chat: Chat? = null,

        @Transient
        @JsonProperty("forward_from")
        var forwardFrom: User? = null,

        @Transient
        @JsonProperty("forward_from_chat")
        var forwardFromChat: Chat? = null,

        @Transient
        @JsonProperty("forward_from_message_id")
        var forwardFromMessageId: Long? = null,

        @Transient
        @JsonProperty("forward_signature")
        var forwardSignature: String? = null,

        @Transient
        @JsonProperty("forward_date")
        var forwardDate: Long? = null,

        @Transient
        @JsonProperty("reply_to_message")
        var replyToMessage: Message? = null,

        @Transient
        @JsonProperty("edit_date")
        var editDate: Long? = null,

        @Transient
        @JsonProperty("media_group_id")
        var mediaGroupId: String? = null,

        @Transient
        @JsonProperty("author_signature")
        var authorSignature: String? = null,

        @JsonProperty
        var text: String? = null,

        @Transient
        @JsonProperty
        var entities: Array<MessageEntity>? = null,

        @Transient
        @JsonProperty("caption_entities")
        var captionEntities: Array<MessageEntity>? = null,

        @Transient
        @JsonProperty
        var audio: Audio? = null,

        @Transient
        @JsonProperty
        var document: Document? = null,

        @Transient
        @JsonProperty
        var game: Game? = null,

        @Transient
        @JsonProperty
        var photo: Array<PhotoSize>? = null,

        @Transient
        @JsonProperty
        var sticker: Sticker? = null,

        @Transient
        @JsonProperty
        var video: Video? = null,

        @Transient
        @JsonProperty
        var voice: Voice? = null,

        @Transient
        @JsonProperty("video_note")
        var videoNote: VideoNote? = null,

        @Transient
        @JsonProperty
        var caption: String? = null,

        @Transient
        @JsonProperty
        var contact: Contact? = null,

        @Transient
        @JsonProperty
        var location: Location? = null,

        @Transient
        @JsonProperty
        var venue: Venue? = null,

        @Transient
        @JsonProperty("new_chat_members")
        var newChatMembers: Array<User>? = null,

        @Transient
        @JsonProperty("left_chat_member")
        var leftChatMember: User? = null,

        @Transient
        @JsonProperty("new_chat_title")
        var newChatTitle: String? = null,

        @Transient
        @JsonProperty("new_chat_photo")
        var newChatPhoto: Array<PhotoSize>? = null,

        @Transient
        @JsonProperty("delete_chat_photo")
        var deleteChatPhoto: Boolean? = null,

        @Transient
        @JsonProperty("group_chat_created")
        var groupChatCreated: Boolean? = null,

        @Transient
        @JsonProperty("super_group_chat_created")
        var superGroupChatCreated: Boolean? = null,

        @Transient
        @JsonProperty("channel_chat_created")
        var channelChatCreated: Boolean? = null,

        @Transient
        @JsonProperty("migrate_to_chat_id")
        var migrateToChatId: Int? = null,

        @Transient
        @JsonProperty("migrate_from_chat_id")
        var migrateFromChatId: Int? = null,

        @Transient
        @JsonProperty("pinnned_message")
        var pinnedMessage: Message? = null,

        @Transient
        @JsonProperty
        var invoice: Invoice? = null,

        @Transient
        @JsonProperty("successful_payment")
        var successfulPayment: SuccessfulPayment? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Message

        if (messageId != other.messageId) return false
        if (from != other.from) return false
        if (date != other.date) return false
        if (chat != other.chat) return false
        if (forwardFrom != other.forwardFrom) return false
        if (forwardFromChat != other.forwardFromChat) return false
        if (forwardFromMessageId != other.forwardFromMessageId) return false
        if (forwardSignature != other.forwardSignature) return false
        if (forwardDate != other.forwardDate) return false
        if (replyToMessage != other.replyToMessage) return false
        if (editDate != other.editDate) return false
        if (mediaGroupId != other.mediaGroupId) return false
        if (authorSignature != other.authorSignature) return false
        if (text != other.text) return false
        if (!Arrays.equals(entities, other.entities)) return false
        if (!Arrays.equals(captionEntities, other.captionEntities)) return false
        if (audio != other.audio) return false
        if (document != other.document) return false
        if (game != other.game) return false
        if (!Arrays.equals(photo, other.photo)) return false
        if (sticker != other.sticker) return false
        if (video != other.video) return false
        if (voice != other.voice) return false
        if (videoNote != other.videoNote) return false
        if (caption != other.caption) return false
        if (contact != other.contact) return false
        if (location != other.location) return false
        if (venue != other.venue) return false
        if (!Arrays.equals(newChatMembers, other.newChatMembers)) return false
        if (leftChatMember != other.leftChatMember) return false
        if (newChatTitle != other.newChatTitle) return false
        if (!Arrays.equals(newChatPhoto, other.newChatPhoto)) return false
        if (deleteChatPhoto != other.deleteChatPhoto) return false
        if (groupChatCreated != other.groupChatCreated) return false
        if (superGroupChatCreated != other.superGroupChatCreated) return false
        if (channelChatCreated != other.channelChatCreated) return false
        if (migrateToChatId != other.migrateToChatId) return false
        if (migrateFromChatId != other.migrateFromChatId) return false
        if (pinnedMessage != other.pinnedMessage) return false
        if (invoice != other.invoice) return false
        if (successfulPayment != other.successfulPayment) return false

        return true
    }

    override fun hashCode(): Int {
        var result = messageId ?: 0
        result = 31 * result + (from?.hashCode() ?: 0)
        result = 31 * result + (date ?: 0)
        result = 31 * result + (chat?.hashCode() ?: 0)
        result = 31 * result + (forwardFrom?.hashCode() ?: 0)
        result = 31 * result + (forwardFromChat?.hashCode() ?: 0)
        result = 31 * result + (forwardFromMessageId ?: 0)
        result = 31 * result + (forwardSignature?.hashCode() ?: 0)
        result = 31 * result + (forwardDate ?: 0)
        result = 31 * result + (replyToMessage?.hashCode() ?: 0)
        result = 31 * result + (editDate ?: 0)
        result = 31 * result + (mediaGroupId?.hashCode() ?: 0)
        result = 31 * result + (authorSignature?.hashCode() ?: 0)
        result = 31 * result + (text?.hashCode() ?: 0)
        result = 31 * result + (entities?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (captionEntities?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (audio?.hashCode() ?: 0)
        result = 31 * result + (document?.hashCode() ?: 0)
        result = 31 * result + (game?.hashCode() ?: 0)
        result = 31 * result + (photo?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (sticker?.hashCode() ?: 0)
        result = 31 * result + (video?.hashCode() ?: 0)
        result = 31 * result + (voice?.hashCode() ?: 0)
        result = 31 * result + (videoNote?.hashCode() ?: 0)
        result = 31 * result + (caption?.hashCode() ?: 0)
        result = 31 * result + (contact?.hashCode() ?: 0)
        result = 31 * result + (location?.hashCode() ?: 0)
        result = 31 * result + (venue?.hashCode() ?: 0)
        result = 31 * result + (newChatMembers?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (leftChatMember?.hashCode() ?: 0)
        result = 31 * result + (newChatTitle?.hashCode() ?: 0)
        result = 31 * result + (newChatPhoto?.let { Arrays.hashCode(it) } ?: 0)
        result = 31 * result + (deleteChatPhoto?.hashCode() ?: 0)
        result = 31 * result + (groupChatCreated?.hashCode() ?: 0)
        result = 31 * result + (superGroupChatCreated?.hashCode() ?: 0)
        result = 31 * result + (channelChatCreated?.hashCode() ?: 0)
        result = 31 * result + (migrateToChatId ?: 0)
        result = 31 * result + (migrateFromChatId ?: 0)
        result = 31 * result + (pinnedMessage?.hashCode() ?: 0)
        result = 31 * result + (invoice?.hashCode() ?: 0)
        result = 31 * result + (successfulPayment?.hashCode() ?: 0)
        return result.toInt()
    }
}


@JsonInclude(JsonInclude.Include.NON_NULL)
class MessageEntity {

    var type: String? = null
    var offset: Int? = null
    var length: Int? = null
    var url: String? = null
    var user: User? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class PhotoSize {

    var fileId: String? = null
    var width: Int? = null
    var height: Int? = null
    var fileSize: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Audio {

    var fileId: String? = null
    var duration: Int? = null
    var performer: String? = null
    var title: String? = null
    var mimeType: String? = null
    var fileSize: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Document {

    var fileId: String? = null
    var thumb: PhotoSize? = null
    var fileName: String? = null
    var mimeType: String? = null
    var fileSize: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Video {

    var fileId: String? = null
    var width: Int? = null
    var height: Int? = null
    var duration: Int? = null
    var thumb: PhotoSize? = null
    var mimeType: String? = null
    var fileSize: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Voice {

    var fileId: String? = null
    var duration: Int? = null
    var mimeType: String? = null
    var fileSize: Int? = null
}

@JsonInclude(JsonInclude.Include.NON_NULL)
class VideoNote {

    var fileId: String? = null
    var length: Int? = null
    var duration: Int? = null
    var thumb: PhotoSize? = null
    var fileSize: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Contact {

    var phoneNumber: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var userId: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Location {

    var longitude: Double? = null
    var latitude: Double? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Venue {

    var location: Location? = null
    var title: String? = null
    var address: String? = null
    var foursquareId: String? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class UserProfilePhotos {

    var totalCount: Int? = null
    var photos: Array<PhotoSize>? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class File {

    var fileId: String? = null
    var fileSize: Int? = null
    var filePath: String? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ReplyKeyboardMarkup {

    var keyboard: Array<KeyboardButton>? = null
    var resizeKeyboard: Boolean? = null
    var oneTimeKeyboard: Boolean? = null
    var selective: Boolean? = null
}

@JsonInclude(JsonInclude.Include.NON_NULL)
class KeyboardButton {

    var text: String? = null
    var requestContact: Boolean? = null
    var requestLocation: Boolean? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ReplyKeyboardRemove {

    var removeKeyboard: Boolean? = null
    var selective: Boolean? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class InlineKeyboardMarkup {

    var inlineKeyboard: Array<InlineKeyboardButton>? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class InlineKeyboardButton {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ChatMember {

    var user: User? = null
    var status: String? = null
    var untilDate: Int? = null
    var canBeEdited: Boolean? = null
    var canChangeInfo: Boolean? = null
    var canPostMessages: Boolean? = null
    var canEditMessages: Boolean? = null
    var canDeleteMessages: Boolean? = null
    var canInviteUsers: Boolean? = null
    var canRestrictMembers: Boolean? = null
    var canPinMessages: Boolean? = null
    var canPromoteMembers: Boolean? = null
    var canSendMessages: Boolean? = null
    var canSendMediaMessages: Boolean? = null
    var canSendOtherMessages: Boolean? = null
    var canAddWebPagePreviews: Boolean? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ResponseParameters {

    var migrateToChatId: Int? = null
    var retryAfter: Int? = null

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class InlineQuery {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ChosenInlineResult {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class CallbackQuery {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ShippingQuery {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class PreCheckoutQuery {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class ChatPhoto {

    var smallFileId: String? = null
    var bigFileId: String? = null
}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Game {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Sticker {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class Invoice {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
class SuccessfulPayment {

}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class Update(
        @JsonProperty("update_id")
        var updateId: Int? = null,

        @JsonProperty
        var message: Message? = null,

        @JsonProperty("edited_message")
        var editedMessage: Message? = null,

        @JsonProperty("channel_post")
        var channelPost: Message? = null,

        @JsonProperty("edited_channel_post")
        var editedChannelPost: Message? = null,

        @JsonProperty("inline_query")
        var inlineQuery: InlineQuery? = null,

        @JsonProperty("chosen_inline_result")
        var chosenInlineResult: ChosenInlineResult? = null,

        @JsonProperty("callback_query")
        var callbackQuery: CallbackQuery? = null,

        @JsonProperty("shipping_query")
        var shippingQuery: ShippingQuery? = null,

        @JsonProperty("pre_checkout_query")
        var preCheckoutQuery: PreCheckoutQuery? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class WebHook(

        @JsonProperty
        var url: String? = null,

        @JsonProperty
        var certificate: InputStream? = null,

        @JsonProperty("max_connections")
        var maxConnections: Int? = null,

        @JsonProperty("allowed_updates")
        var allowedUpdates: Array<String>? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as WebHook

        if (url != other.url) return false
        if (certificate != other.certificate) return false
        if (maxConnections != other.maxConnections) return false
        if (!Arrays.equals(allowedUpdates, other.allowedUpdates)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = url?.hashCode() ?: 0
        result = 31 * result + (certificate?.hashCode() ?: 0)
        result = 31 * result + (maxConnections ?: 0)
        result = 31 * result + (allowedUpdates?.let { Arrays.hashCode(it) } ?: 0)
        return result
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class BotResponse(

        @JsonProperty("chat_id")
        var chatId: Int? = null,

        @JsonProperty
        var text: String? = null,

        @JsonProperty("parse_mode")
        var parseMode: String? = null,

        @JsonProperty("disable_web_page_preview")
        var disableWebPagePreview: Boolean? = null,

        @JsonProperty("disable_notification")
        var disableNotification: Boolean? = null,

        @JsonProperty("reply_to_message_id")
        var replyToMessageId: Int? = null,

        @JsonProperty("reply_markup")
        var replyMarkup: InlineKeyboardMarkup? = null
)


@JsonInclude(JsonInclude.Include.NON_NULL)
data class Subreddit(

        @JsonProperty
        var kind: String? = null,

        @JsonProperty
        var data: SubredditData? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditData(

        @JsonProperty("modhash")
        var modHash: String? = null,

        @JsonProperty("whitelist_status")
        var whitelistStatus: String? = null,

        @JsonProperty
        var children: Array<SubredditPost>? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SubredditData

        if (modHash != other.modHash) return false
        if (whitelistStatus != other.whitelistStatus) return false
        if (!Arrays.equals(children, other.children)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = modHash?.hashCode() ?: 0
        result = 31 * result + (whitelistStatus?.hashCode() ?: 0)
        result = 31 * result + (children?.let { Arrays.hashCode(it) } ?: 0)
        return result
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditPost(

        @JsonProperty
        var kind: String? = null,

        @JsonProperty
        var data: SubredditPostData? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditPostData(

        @JsonProperty("selftext_html")
        var selfTextHtml: String? = null,

        @JsonProperty("selftext")
        var selfText: String? = null,

        @JsonProperty
        var url: String? = null,

        @JsonProperty
        var title: String? = null,

        @JsonProperty("is_self")
        var isSelf: Boolean? = null,

        @JsonProperty("over_18")
        var over18: Boolean? = null,

        @JsonProperty
        var gilded: Int? = null,

        @JsonProperty
        var permalink: String? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditCommentPost(

        @JsonProperty
        var kind: String? = null,

        @JsonProperty
        var data: SubredditCommentPostData? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditCommentPostData(

        @JsonProperty
        var modhash: String? = null,

        @JsonProperty("whitelist_status")
        var whitelistStatus: String? = null,

        @JsonProperty
        var children: Array<SubredditComment>? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SubredditCommentPostData

        if (modhash != other.modhash) return false
        if (whitelistStatus != other.whitelistStatus) return false
        if (!Arrays.equals(children, other.children)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = modhash?.hashCode() ?: 0
        result = 31 * result + (whitelistStatus?.hashCode() ?: 0)
        result = 31 * result + (children?.let { Arrays.hashCode(it) } ?: 0)
        return result
    }
}

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditComment(

        @JsonProperty
        var kind: String? = null,

        @JsonProperty
        var data: SubredditCommentData? = null
)

@JsonInclude(JsonInclude.Include.NON_NULL)
data class SubredditCommentData(

        @JsonProperty
        var ups: String? = null,

        @JsonProperty
        var body: String? = null
)
