@file:Suppress("UNCHECKED_CAST")

package xyz.nixqloud.bot

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment
import xyz.nixqloud.bot.service.HttpService
import xyz.nixqloud.bot.service.TelegramService
import xyz.nixqloud.bot.service.TelegramServiceImpl
import xyz.nixqloud.bot.service.TelegramServiceMockImpl
import java.net.URLEncoder


@SpringBootApplication
class TelegramApplication {

    @Bean
    fun telegramService(httpService: HttpService, objectMapper: ObjectMapper, env: Environment): TelegramService {

        val activeProfile = env.getProperty("spring.profiles.active")

        return if (activeProfile == "prod") {
            TelegramServiceImpl(httpService, objectMapper, env)
        } else {
            TelegramServiceMockImpl()
        }
    }
}


fun main(args: Array<String>) {

    val context = SpringApplication.run(TelegramApplication::class.java)
    val env = context.getBean("environment", Environment::class.java)
    val activeProfile = env.getProperty("spring.profiles.active")

    if (activeProfile == "prod") {

        val log = LoggerFactory.getLogger(TelegramApplication::class.java)
        val httpService = context.getBean("httpService", HttpService::class.java)

        val deregisterBotUrl = env.getProperty("telegram.bot.deregister-url")
        val urlPieces = env.getProperty("telegram.bot.register-url").split("=")
        val registerBotUrl = "${urlPieces[0]}=${URLEncoder.encode(urlPieces[1], "UTF-8")}"

        val deregisterResponse = httpService.get(deregisterBotUrl)
        val registerResponse = httpService.get(registerBotUrl)

        log.info("main() - deregister response: $deregisterResponse")
        log.info("main() - register response: $registerResponse")
    }
}
