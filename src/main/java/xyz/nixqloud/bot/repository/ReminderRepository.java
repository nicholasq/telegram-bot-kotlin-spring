package xyz.nixqloud.bot.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.nixqloud.bot.entity.Reminder;

public interface ReminderRepository extends CrudRepository<Reminder, Long> {
}
