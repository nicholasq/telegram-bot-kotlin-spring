package xyz.nixqloud.bot.repository;

import org.springframework.data.repository.CrudRepository;
import xyz.nixqloud.bot.json.Message;

public interface MessageRepository extends CrudRepository<Message, Long> {
    Message findByMessageId(long messageId);
}
