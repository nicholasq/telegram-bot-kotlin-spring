package xyz.nixqloud.bot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.entity.Reminder;
import xyz.nixqloud.bot.json.BotResponse;
import xyz.nixqloud.bot.repository.ReminderRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service("reminderFetchService")
public class ReminderFetchServiceImpl implements ReminderFetchService {

    private final TelegramService telegramService;
    private final ReminderRepository reminderRepository;

    private final Logger log = LoggerFactory.getLogger(ReminderFetchService.class);
    private static final String FORMAT = "Hey [You](mention:%s)\nReminder: %s\nFor: %s";

    @Autowired
    public ReminderFetchServiceImpl(TelegramService telegramService, ReminderRepository reminderRepository) {
        this.telegramService = telegramService;
        this.reminderRepository = reminderRepository;
    }

    @Scheduled(fixedRate = 60 * 1000) // Every minute
    @Override
    public void check() {

        try {
            findReminders();
        } catch (Exception e) {
            log.error("check() - error happened while retrieving reminders", e);
        }
    }

    private void findReminders() {

        LocalDateTime now = LocalDateTime.now(ZoneId.of("America/Phoenix"));
        Iterable<Reminder> reminders = reminderRepository.findAll();
        List<Reminder> readyReminders = StreamSupport
                .stream(reminders.spliterator(), false)
                .collect(Collectors.toMap(Reminder::getDateTime, it -> it))
                .entrySet()
                .stream()
                .filter(it -> it.getKey().getYear() == now.getYear()
                        && it.getKey().getMonth() == now.getMonth()
                        && it.getKey().getDayOfMonth() == now.getDayOfMonth()
                        && it.getKey().getHour() == now.getHour()
                        && it.getKey().getMinute() <= now.getMinute())
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());

        for (Reminder reminder : readyReminders) {

            String text = String.format(
                    FORMAT,
                    reminder.getUserId(),
                    reminder.getDateTime().toString(),
                    reminder.getBody()
            );
            BotResponse botResponse = new BotResponse();
            botResponse.setChatId(reminder.getChatId());
            botResponse.setText(text);
            botResponse.setParseMode("Markdown");

            telegramService.sendMessage(botResponse);

            log.info("check() - delete reminder: {}", reminder);

            reminderRepository.delete(reminder);
        }
    }
}
