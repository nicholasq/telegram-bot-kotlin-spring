package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.json.BotResponse;
import xyz.nixqloud.bot.json.Update;

import java.io.IOException;
import java.util.regex.Pattern;

@Service("cryptoService")
public class CryptoService implements BasicBotService {

    public static final String COMMAND = "/crypto";
    private static final String queryUrl = "https://min-api.cryptocompare.com/data/price?fsym=%s&tsyms=USD";
    private static final Logger log = LoggerFactory.getLogger(CryptoService.class);
    private static final Pattern alphaPattern = Pattern.compile("^[a-zA-Z]{1,4}$");
    private final HttpService httpService;
    private final ObjectMapper jsonMapper;

    @Autowired
    public CryptoService(HttpService httpService, ObjectMapper jsonMapper) {
        this.httpService = httpService;
        this.jsonMapper = jsonMapper;
    }

    @NotNull
    @Override
    public BotResponse handleUpdate(@NotNull Update update) {

        // Strip out the command to get symbol. The api we use requires uppercase symbols.
        String symbol = update.getMessage().getText().substring(COMMAND.length() + 1).trim().toUpperCase();
        BotResponse botResponse = new BotResponse();
        botResponse.setChatId(update.getMessage().getChat().getId());

        if (symbol.length() > 4 || !alphaPattern.matcher(symbol).matches()) {
            botResponse.setText("Your crypto symbol can't be over 4 characters and it can only be letters");
        } else {

            // Use httpService to do a http GET request which that particular site will return
            // the response as json.
            String response = httpService.get(String.format(queryUrl, symbol));
            String result;

            try {
                /*
                 * Convert the string into a Java representation of a Json object.
                 * The json that came from crytpo-compare.com looks like this:
                 * {"BTC":0.08425,"USD":1238.91,"EUR":1040.06}
                 * A better formatted version looks like this:
                 * {
                 *   "BTC":0.08425,
                 *   "USD":1238.91,
                 *   "EUR":1040.06
                 * }
                 *
                 * However, if there is an error like a crypto symbol cannot be found, the json will look like this:
                 * {"Response":"Error","Message":"There is no data for the symbol btc .","Type":1,"Aggregated":false,"Data":[]}
                 *
                 * So we need to make sure that we process the correct response accordingly.
                 */
                JsonNode jsonNode = jsonMapper.readTree(response);
                if (jsonNode.has("Response") && jsonNode.has("Message")) {
                    result = jsonNode.get("Message").asText();
                } else if (jsonNode.has("USD")) {
                    // Read the USD property of the json object.
                    result = symbol + ": " + jsonNode.get("USD").asText();
                } else {
                    // We log this as a warn because it's out of the norm and something we'll want to look into
                    // if it happens.
                    log.warn("handleUpdate() - received unexpected json : {}", response);
                    result = "Sorry. Something went wrong";
                }
            } catch (IOException e) {
                // Log exceptions, do not print stacktraces.
                log.error("handleUpdate() - ", e);
                result = "Sorry. Something went wrong";
            }
            botResponse.setText(result);
        }
        return botResponse;
    }
}
