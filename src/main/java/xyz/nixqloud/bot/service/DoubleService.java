package xyz.nixqloud.bot.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.json.BotResponse;
import xyz.nixqloud.bot.json.Update;

@Service("doubleService")
public class DoubleService implements BasicBotService{

    @NotNull
    @Override
    public BotResponse handleUpdate(@NotNull Update update) {

        // Here you retrieve the text that came from the message
        String text = update.getMessage().getText();

        // Process the text how you want.
        String subStringResponse = text.substring(7);
        String someResponse = subStringResponse + subStringResponse;

        // Now all you need to do is bundle it up in a BotResponse object.
        BotResponse botResponse = new BotResponse();

        // Make sure you set the chat id or your message won't be delivered to
        // the chat.
        botResponse.setChatId(update.getMessage().getChat().getId());

        // Now set your processed text.
        botResponse.setText(someResponse);

        // Return your botresponse.
        return botResponse;
    }
}
