package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.json.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("redditService")
public final class RedditService implements BasicBotService, MessageInterceptorService {

    public static final String COMMAND = "/reddit";
    private static final String[] modifiers;
    private static final String BASE_URL = "https://reddit.com/r/%s/%s.json"; // example: https://reddit.com/r/pics/hot.json
    private static final Logger log = LoggerFactory.getLogger(RedditService.class);
    private static final Map<String, String> headers;
    private static final String INVALID_MODIFIER_WARNING;
    private static final Random random = new Random();
    private static final Pattern mpdFilePattern = Pattern.compile(".*(https://.*redd.*it.*\\.mpd).*");

    private final HttpService httpService;
    private final ObjectMapper jsonMapper;

    static {
        String[] items = {"new", "hot", "rising", "random", "top", "controversial"};
        Arrays.sort(items);
        modifiers = items;
        headers = new HashMap<>();
        headers.put("User-Agent", "spring:xyz.nixqloud.bot:v0.1");
        INVALID_MODIFIER_WARNING = "Sorry but you can only use one of these: " + String.join(", ", modifiers);
    }

    @Autowired
    public RedditService(HttpService httpService, ObjectMapper jsonMapper) {
        this.httpService = httpService;
        this.jsonMapper = jsonMapper;
    }

    @NotNull
    @Override
    public BotResponse handleUpdate(@NotNull Update update) {

        String text = update.getMessage().getText();
        if (!text.startsWith(COMMAND)) {
            return fixMpdFile(update);
        }
        // Break up the incoming message into arguments split by whitespace.
        // A valid command would be: /reddit <subreddit>
        // So for example: /reddit pics
        final String[] args = text.split("\\s");
        final BotResponse botResponse = new BotResponse();
        botResponse.setChatId(update.getMessage().getChat().getId());

        if (args.length < 2) {
            botResponse.setText("You need to specify a subreddit");
            return botResponse;
        }

        final String[] subredditArgs = args[1].split("/");
        final String subreddit = subredditArgs[0].toLowerCase();
        final String builtUrl;

        // This means they want to add a modifier like pics/top or pics/new
        if (subredditArgs.length == 2) {

            String modifier = subredditArgs[1].toLowerCase();
            int index = Arrays.binarySearch(modifiers, modifier);

            // If they chose an invalid modifier we need to let them know.
            if (index == -1) {
                botResponse.setText(INVALID_MODIFIER_WARNING);
                return botResponse;
            }

            builtUrl = String.format(BASE_URL, subreddit, modifier);

        } else {
            // They didn't specify a modifier so we'll default to hot.
            builtUrl = String.format(BASE_URL, subreddit, "hot");
        }

        SubredditPostData subredditPost = retrieveRandomSubredditPost(builtUrl);

        if (subredditPost == null) {
            botResponse.setText("Sorry but " + subreddit + " was not found");
            return botResponse;
        }

        String title = subredditPost.getTitle();
        String selfText = subredditPost.getSelfText();
        String url = subredditPost.getUrl();
        String permaLink = "https://reddit.com" + subredditPost.getPermalink();

        String format = "*%s*\n\n%s\n\n[View post here](%s)";
        String body = url != null && !url.equals("") ? url : selfText;

        // Make sure a NSFW pic does not autoload in telegram.
        botResponse.setDisableWebPagePreview(subredditPost.getOver18());
        botResponse.setText(String.format(format, title, body, permaLink));
        botResponse.setParseMode("Markdown");

        return botResponse;
    }

    private BotResponse fixMpdFile(Update update) {

        String text = update.getMessage().getText();
        Matcher matcher = mpdFilePattern.matcher(text);

        BotResponse botResponse = new BotResponse();

        if (matcher.matches()) {
            String url = matcher.group(1);
            String fixed = url.substring(0, url.lastIndexOf("/"));
            System.out.println(fixed);
            botResponse.setText("I got you:\n" + fixed);
            return botResponse;
        } else {
            return botResponse;
        }
    }

    public Subreddit retrieveRedditPage(String url) {

        String json = httpService.get(url, headers);
        try {
            return jsonMapper.readValue(json, Subreddit.class);
        } catch (IOException e) {
            log.error("retrieveRedditPage() - could not parse json response: {}", json, e);
            return new Subreddit();
        }
    }

    // todo view the actual json to see if it's a post or postdata
    public SubredditPost retrieveSubRedditPost(String url) {

        String json = httpService.get(url, headers);
        try {
            return jsonMapper.readValue(json, SubredditPost.class);
        } catch (IOException e) {
            log.error("retrieveSubRedditPost() - could not parse json response: {}", json, e);
            return new SubredditPost();
        }
    }

    // todo: don't like call to retrieveredditpage. have caller supply needed subredditpage
    public SubredditPostData retrieveRandomSubredditPost(String url) {

        // Ok creating the url went good. Time to visit reddit and get the data.
        Subreddit subredditPage = retrieveRedditPage(url);

        /*
         * It seems that even if a subreddit does not exist, reddit will still return what looks
         * like a valid reddit page json object .
         * So we have to check a for null down a few levels and that children (the posts) aren't empty.
         */
        if (subredditPage != null
                && subredditPage.getData() != null
                && subredditPage.getData().getChildren() != null
                && subredditPage.getData().getChildren().length > 0) {

            // todo add some sort of randomization in choosing a page and maybe allow user to choose.
            SubredditPost[] children = subredditPage.getData().getChildren();
            int randInt = random.nextInt(children.length);

            return children[randInt].getData();
        } else {
            return null;
        }

    }

    // todo: signature is ugly. redo json types
    public SubredditCommentPostData retrieveSubredditCommentPostData(String url) {

        String jsonString = httpService.get(url, headers);
        try {
            JsonNode rootNode = jsonMapper.readTree(jsonString);
            SubredditCommentPost commentPost = jsonMapper.treeToValue(rootNode.get(1), SubredditCommentPost.class);
            return commentPost.getData();
        } catch (IOException e) {
            log.error("retrieveSubredditCommentPostData() - error when getting commentpostdata for: {}", url, e);
            return new SubredditCommentPostData();
        }
    }

}
