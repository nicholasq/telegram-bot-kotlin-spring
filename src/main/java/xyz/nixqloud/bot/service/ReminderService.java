package xyz.nixqloud.bot.service;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.entity.Reminder;
import xyz.nixqloud.bot.json.BotResponse;
import xyz.nixqloud.bot.json.Update;
import xyz.nixqloud.bot.repository.ReminderRepository;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("reminderService")
public class ReminderService implements BasicBotService {

    public static final String COMMAND = "/reminder";
    private static final Pattern dateFormatPattern = Pattern.compile("^on (\\d{1,2} \\d{1,2} \\d{4} \\d{2}:\\d{2}[AP]M)");
    private static final Pattern numberFormatPattern = Pattern.compile("^\\d+$");
    private static final Pattern timeFormatPattern = Pattern.compile("^(sec|min|hour|day|week|month|year)s?$");
    private static final Logger log = LoggerFactory.getLogger(ReminderService.class);
    private static final String format_error_message = "Format not understood. Try:\n" +
            "in 5 (mins|hours|days)\n" +
            "or\n" +
            "on feb 14";
    private final ReminderRepository reminderRepository;

    @Autowired
    public ReminderService(ReminderRepository reminderRepository) {
        this.reminderRepository = reminderRepository;
    }

    @NotNull
    @Override
    public BotResponse handleUpdate(@NotNull Update update) {

        /*
         * We need to decide here what the expected format should be for saving a reminder.
         * That way we know how to parse it and break it up into individual components.
         * Things to think about... what's the format for a date that the user should use?
         * For example: Jan 5 2018 ???
         *
         * Then we need to parse their reminder content.
         *
         * Next would be sticking our parsed data into a Reminder object and persisting to
         * a database.
         *
         * Let the user know if all went well or if there was an issue. Be descriptive on what
         * exactly they did wrong.
         *
         * Last part is creating a batch service that queries the reminders table to see if a
         * reminder needs to be posted to the chat. We can also make it so the reminder will
         * start warning when the time is getting close. For example, letting the chat know
         * their scheduled dinner date is coming up in an hour.
         */
        BotResponse botResponse = new BotResponse();
        String text = update.getMessage().getText().toLowerCase();
        String result;

        if (text.contains(COMMAND + " on")) {
            result = processOn(update);
        } else if (text.contains(COMMAND + " in")) {
            result = processIn(update);
        } else {
            result = format_error_message;
        }
        botResponse.setText(result);
        return botResponse;
    }

    private String processOn(Update update) {

        int userId = update.getMessage().getFrom().getId();
        int chatid = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();
        String requestedReminder = text.substring(COMMAND.length()).trim();
        Matcher matcher = dateFormatPattern.matcher(requestedReminder);

        if (matcher.find()) {

            String dateString = matcher.group(1);
            LocalDateTime localDateTime = LocalDateTime.parse(
                    dateString,
                    DateTimeFormatter.ofPattern("MM dd yyyy hh:mma")
            );
            String body = requestedReminder.substring("on ".length() + dateString.length());
            Reminder reminder = new Reminder(localDateTime, userId, body, chatid);

            return saveReminder(reminder);
        } else {
            return format_error_message;
        }
    }

    private String processIn(Update update) {

        String user = update.getMessage().getChat().getUsername();
        int chatid = update.getMessage().getChat().getId();
        String text = update.getMessage().getText();
        String[] args = text.substring(COMMAND.length()).trim().split("\\s");

        if (args.length < 5) {
            return "you need to specify a time range. ex: /reminder in 5 mins";
        } else if (!args[0].equals("in")) {
            return "not a valid format. ex: /reminder in 5 mins";
        } else if (!numberFormatPattern.matcher(args[1]).matches()) {
            return "not a valid format. ex: /reminder in 5 mins";
        } else if (!timeFormatPattern.matcher(args[2]).matches()) {
            return "not a valid format. ex: /reminder in 5 mins";
        } else {

            int qty = Integer.parseInt(args[1]);
            String timeType = args[2].toLowerCase();
            LocalDateTime now = LocalDateTime.now();

            if (timeType.charAt(timeType.length() - 1) == 's') {
                timeType = timeType.substring(0, timeType.length() - 1);
            }

            LocalDateTime requestedTime;

            switch (timeType) {
                case "sec":
                    requestedTime = now.plusSeconds(qty);
                    break;
                case "min":
                    requestedTime = now.plusSeconds(qty);
                    break;
                case "hour":
                    requestedTime = now.plusSeconds(qty);
                    break;
                case "day":
                    requestedTime = now.plusSeconds(qty);
                    break;
                case "week":
                    requestedTime = now.plusSeconds(qty);
                    break;
                case "mon":
                    requestedTime = now.plusSeconds(qty);
                    break;
                default:
                    return "Sorry something went wrong";
            }

            String body = Arrays.stream(Arrays.copyOfRange(args, 5, args.length))
                    .reduce((a, b) -> a + "" + b)
                    .orElse("");
            if (body.equals("")) {
                return "You need a body to your reminder";
            }

            Reminder reminder = new Reminder(
                    requestedTime,
                    update.getMessage().getFrom().getId(),
                    body,
                    update.getMessage().getChat().getId()
            );


            return saveReminder(reminder);
        }

    }

    private String saveReminder(Reminder reminder) {

        try {
            Reminder saved = reminderRepository.save(reminder);
            log.info("handleUpdate() - reminder persisted: {}", saved);
            return "reminder saved";
        } catch (Exception e) {

            log.error("handleUpdate() - error happened while persisting reminder: {}", reminder, e);
            return "Sorry, something went wrong when trying to save your reminder";
        }
    }
}
