package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.json.*;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;
import java.util.Random;

@Service("botThoughtService")
public class BotThoughtService implements BasicBotService {

    public static final String COMMAND = "/whatareyouthinking";
    public static final ObjectMapper jsonMapper = new ObjectMapper();
    private static final String[] subreddits = {"redpill", "psychology", "science", "eli5"};
    private static final Random random = new Random();
    private final RedditService redditService;

    @Autowired
    public BotThoughtService(RedditService redditService) {
        this.redditService = redditService;
    }

    @NotNull
    @Override
    public BotResponse handleUpdate(@NotNull Update update) {

        BotResponse botResponse = new BotResponse();
        String subredditStr = subreddits[random.nextInt(subreddits.length)];
        String subredditUrl = "https://reddit.com/r/" + subredditStr + "/hot.json";
        SubredditCommentPostData subredditCommentPostData = redditService.retrieveSubredditCommentPostData(subredditUrl);
        Optional<SubredditCommentData> comment = Arrays.stream(subredditCommentPostData.getChildren())
                .map(SubredditComment::getData)
                .max(Comparator.comparing(SubredditCommentData::getUps));
        String text = comment.map(SubredditCommentData::getUps).orElse("");
        botResponse.setText(text);
        return botResponse;
    }
}
