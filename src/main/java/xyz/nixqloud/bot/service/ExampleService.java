package xyz.nixqloud.bot.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import xyz.nixqloud.bot.json.BotResponse;
import xyz.nixqloud.bot.json.Update;

/**
 * This class is an example of how to create a "plugin" for
 * the bot. For a class to be used by the bot, it needs to implement
 * the BasicBotService interface as shown below.
 *
 * Once you've done all that, you need to register this service in 2 ways.
 * First annotate your class with @Service("give a name here") as shown below.
 *
 * Then you need to determine how your service is "selected" by adding an
 * an entry to the services map in MessageInterpreterServiceImpl.kt.
 *
 * Here's a simple version of that:
 *
 * private val services = mapOf(
 * "/stock" to "stockService",
 * "/ping" to "pingService"
 * )
 *
 * So for example, to have our ExampleService be called when the command: /example
 * is received from a user, we add it like this:
 *
 * private val services = mapOf(
 * "/stock" to "stockService",
 * "/ping" to "pingService",
 * "/example" to "exampleService"
 * )
 *
 * Pay close attention that you are adding the key which is your command,
 * and the value which is the same string in your @Service annotation
 *
 */
@Service("exampleService")
public class ExampleService implements BasicBotService {

    public static final String COMMAND = "/example";

    @NotNull
    @Override
    public BotResponse handleUpdate(@NotNull Update update) {

        // Here you retrieve the text that came from the message
        String text = update.getMessage().getText();

        // Process the text how you want.
        String someResponse = "I am the exampleService! I received: " + text;

        // Now all you need to do is bundle it up in a BotResponse object.
        BotResponse botResponse = new BotResponse();

        // Make sure you set the chat id or your message won't be delivered to
        // the chat.
        botResponse.setChatId(update.getMessage().getChat().getId());

        // Now set your processed text.
        botResponse.setText(someResponse);

        // Return your botresponse.
        return botResponse;
    }
}
