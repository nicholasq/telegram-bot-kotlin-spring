package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import xyz.nixqloud.bot.TelegramApplicationTest;
import xyz.nixqloud.bot.TestHelpersKt;
import xyz.nixqloud.bot.json.Chat;
import xyz.nixqloud.bot.json.Message;
import xyz.nixqloud.bot.json.Update;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TelegramApplicationTest.class})
public class RedditServiceTests {

    @Test
    public void testValidSubreddit() throws Exception {

        Chat chat = new Chat();
        Message message = new Message();
        Update update = new Update();
        String subreddit = "pics";

        chat.setId(1234);
        message.setChat(chat);
        message.setText("/reddit " + subreddit);
        update.setMessage(message);

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text", is(not(""))));
    }

    @Test
    public void testInvalidSubreddit() throws Exception {

        Chat chat = new Chat();
        Message message = new Message();
        Update update = new Update();
        String subreddit = "12445415431"; // Assuming there's no subreddit called this.

        chat.setId(1234);
        message.setChat(chat);
        message.setText("/reddit " + subreddit);
        update.setMessage(message);

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text", containsString("Sorry but " + subreddit + " was not found")));

        chat.setId(1234);
        message.setChat(chat);
        message.setText("https://v.redd.it/ngks9roqif801/DASHPlaylist.mpd" + subreddit);
        update.setMessage(message);

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk());
    }

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webAppContext;

    @Value("${telegram.bot.webhook-path}")
    private String postUrl;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webAppContext).build();
    }
}