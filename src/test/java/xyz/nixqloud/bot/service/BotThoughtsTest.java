package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.nixqloud.bot.TelegramApplicationTest;
import xyz.nixqloud.bot.json.SubredditComment;
import xyz.nixqloud.bot.json.SubredditCommentPost;
import xyz.nixqloud.bot.json.SubredditCommentPostData;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TelegramApplicationTest.class})
public class BotThoughtsTest {

    @SuppressWarnings("unchecked")
    @Test
    public void testjson() throws IOException {

        // todo: figure out how to make reliable tests with data that can change or
        // consider moving this json into a file and reading from there.
        String jsonString = httpService.get("https://www.reddit.com/r/gifs/comments/7q1qft/what_just_happened/.json", headers);
        JsonNode rootNode = jsonMapper.readTree(jsonString);
        SubredditCommentPost commentPost = jsonMapper.treeToValue(rootNode.get(1), SubredditCommentPost.class);
        SubredditCommentPostData commentsPostData = commentPost.getData();
        SubredditComment[] children = commentsPostData.getChildren();
        String format = "ups: %s\nbody:\n%s";

        for (SubredditComment comment : children) {
            String body = comment.getData().getBody();
            String ups = comment.getData().getUps();
            System.out.println(String.format(format, ups, body));
        }

    }

    private static final Map<String, String> headers = new HashMap<>();

    static {
        headers.put("User-Agent", "spring:xyz.nixqloud.bot:v0.1");
    }

    @Autowired
    public HttpService httpService;

    @Autowired
    public ObjectMapper jsonMapper;
}
