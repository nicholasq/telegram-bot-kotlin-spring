package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import xyz.nixqloud.bot.TelegramApplicationTest;
import xyz.nixqloud.bot.TestHelpersKt;
import xyz.nixqloud.bot.json.Chat;
import xyz.nixqloud.bot.json.Message;
import xyz.nixqloud.bot.json.Update;
import xyz.nixqloud.bot.json.User;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TelegramApplicationTest.class})
public class ReminderServiceTests {

    private static final String REMINDER_SUCCESSFULLY_SAVED = "reminder saved";

    @Test
    public void testInvalidFormat() throws Exception {

        Chat chat = new Chat();
        Message message = new Message();
        Update update = new Update();
        User user = new User();

        user.setId(987654);
        chat.setId(1234);
        message.setChat(chat);
        message.setText("/reminder gibberish here");
        message.setFrom(user);
        update.setMessage(message);

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text", is("Format not understood. Try:\nin 5 (mins|hours|days)\nor\non feb 14")));

        message.setText("/reminder on 3324123 30 2018");

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text", is("Format not understood. Try:\nin 5 (mins|hours|days)\nor\non feb 14")));
    }

    @Test
    public void testValidFormat() throws Exception {

        Chat chat = new Chat();
        Message message = new Message();
        Update update = new Update();
        User user = new User();
        String requestedDate = "/reminder on 01 18 2018 01:30PM" + " \ndinner with friends";

        user.setId(987654);
        chat.setId(1234);
        message.setChat(chat);
        message.setText(requestedDate);
        message.setFrom(user);
        update.setMessage(message);

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk())
                .andDo(TestHelpersKt.getPrintResponse())
                .andExpect(jsonPath("$.text", is(REMINDER_SUCCESSFULLY_SAVED)));

    }

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webAppContext;

    @Value("${telegram.bot.webhook-path}")
    private String postUrl;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webAppContext).build();
    }
}