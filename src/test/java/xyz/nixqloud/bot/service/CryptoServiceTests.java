package xyz.nixqloud.bot.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import xyz.nixqloud.bot.TelegramApplicationTest;
import xyz.nixqloud.bot.TestHelpersKt;
import xyz.nixqloud.bot.json.Chat;
import xyz.nixqloud.bot.json.Message;
import xyz.nixqloud.bot.json.Update;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TelegramApplicationTest.class})
public class CryptoServiceTests {

    @Test
    public void testThatPriceIsRetrieved() throws Exception {

        Chat chat = new Chat();
        Message message = new Message();
        Update update = new Update();
        String symbol = "btc"; // Bitcoin.
        /*
         * A Message object holds a Chat object. An Update object holds a message object.
         * The hierarchy looks like this:
         * Update -> Message -> Chat.
         *
         * So it's best practice to set all your values to the inner most object and work your way outwards.
         */
        chat.setId(1234);
        message.setChat(chat);
        message.setText("/crypto " + symbol);
        update.setMessage(message);

        /*
         * Now what this does is simulate an http post request to our application.
         * The incoming request is the Update object you created above but spring
         * uses the jackson library which turns it into json. So our BotController receives
         * this, then hands it off to MessageInterpreterService which then decides which
         * service this incoming object goes to based on the command you mapped to your service.
         *
         */
        mockMvc.perform(post(postUrl) // Sets up the url to do an http post to.
                .content(jsonMapper.writeValueAsString(update)) // puts the json string into the request.
                .contentType(TestHelpersKt.getJsonContentType())) // sets http header to content-type: application/json
                .andExpect(status().isOk()) // http post is performed and we check the response is ok
                .andExpect(jsonPath("$.text", containsString(symbol.toUpperCase() + ": "))); // now check the actual returned json object.
        /*
         * The price is constantly changing so we can't check for the exact string coming back.
         * Notice we use "containsString()"
         * We'll assume that if "BTC: " comes back, all went well.
         */
    }

    @Test
    public void testInvalidSymbols() throws Exception {

        Chat chat = new Chat();
        Message message = new Message();
        Update update = new Update();
        String symbol = "ZZZZ"; // Assuming there's no symbol called ZZZ

        chat.setId(1234);
        message.setChat(chat);
        message.setText("/crypto " + symbol);
        update.setMessage(message);

        mockMvc.perform(post(postUrl)
                .content(jsonMapper.writeValueAsString(update))
                .contentType(TestHelpersKt.getJsonContentType()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.text", containsString("There is no data for the symbol " + symbol + " .")));
    }

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webAppContext;

    @Value("${telegram.bot.webhook-path}")
    private String postUrl;

    @Autowired
    private ObjectMapper jsonMapper;

    @Before
    public void setup() {
        mockMvc = webAppContextSetup(webAppContext).build();
    }
}