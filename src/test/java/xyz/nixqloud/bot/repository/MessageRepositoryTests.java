package xyz.nixqloud.bot.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.nixqloud.bot.TelegramApplicationTest;
import xyz.nixqloud.bot.json.Message;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {TelegramApplicationTest.class})
public class MessageRepositoryTests {

    @Test
    public void testUpdatePersists() {

        Message message1 = new Message();
        message1.setMessageId(12345L);
        message1.setText("hello there");

        messageRepository.save(message1);

        Message retrievedMessage = messageRepository.findByMessageId(12345L);

        assertThat(retrievedMessage.getText(), is("hello there"));
    }

    @Autowired
    private MessageRepository messageRepository;
}
