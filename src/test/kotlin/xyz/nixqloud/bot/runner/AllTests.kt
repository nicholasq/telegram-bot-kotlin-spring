package xyz.nixqloud.bot.runner

import org.junit.runner.RunWith
import org.junit.runners.Suite
import xyz.nixqloud.bot.controller.BotControllerTests
import xyz.nixqloud.bot.repository.MessageRepositoryTests
import xyz.nixqloud.bot.service.*

@RunWith(Suite::class)
@Suite.SuiteClasses(
        value = [
            BotControllerTests::class,
            StockServiceTests::class,
            DoubleServiceTests::class,
            CryptoServiceTests::class,
            BotThoughtsTest::class,
            MessageRepositoryTests::class,
            ReminderServiceTests::class,
            RedditServiceTests::class,
            CommandServiceTests::class,
            PythonExtensionServiceTests::class
        ]
)
class AllTests {
}