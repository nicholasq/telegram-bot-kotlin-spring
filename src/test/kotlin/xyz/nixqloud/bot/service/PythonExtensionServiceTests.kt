package xyz.nixqloud.bot.service

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import xyz.nixqloud.bot.TelegramApplicationTest

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [TelegramApplicationTest::class])
class PythonExtensionServiceTests {

    @Test
    fun `stub test`() {
        // todo implement
    }

    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var webAppContext: WebApplicationContext

    @Value("\${telegram.bot.webhook-path}")
    private lateinit var postUrl: String

    @Before
    fun setup() {
        mockMvc = webAppContextSetup(webAppContext).build()
    }
}