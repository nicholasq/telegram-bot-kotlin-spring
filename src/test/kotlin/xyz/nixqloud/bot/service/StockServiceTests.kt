package xyz.nixqloud.bot.service

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.containsString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import xyz.nixqloud.bot.TelegramApplicationTest
import xyz.nixqloud.bot.json.Chat
import xyz.nixqloud.bot.json.Message
import xyz.nixqloud.bot.json.Update
import xyz.nixqloud.bot.jsonContentType

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [TelegramApplicationTest::class])
class StockServiceTests {

    @Test
    fun `test invalid stock returns correct error message`() {

        val noStockSymbol = Update(
                updateId = 12345,
                message = Message(
                        chat = Chat(
                                id = 54321
                        ),
                        text = "/stock"
                )
        )

        mockMvc.perform(post(postUrl)
                .content(json(noStockSymbol))
                .contentType(jsonContentType))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.text", `is`("You need to specify a stock symbol. example: AMD")))


        val invalidSymbol = Update(
                updateId = 12345,
                message = Message(
                        chat = Chat(
                                id = 54321
                        ),
                        text = "/stock amddd"
                )
        )

        mockMvc.perform(post(postUrl)
                .content(json(invalidSymbol))
                .contentType(jsonContentType))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.text", `is`("Not a valid stock format. Symbol must be at least one character, but no more than four")))
    }

    @Test
    fun `test valid stock retrieval`() {

        val validSymbol = Update(
                updateId = 12345,
                message = Message(
                        chat = Chat(
                                id = 54321
                        ),
                        text = "/stock amd"
                )
        )

        mockMvc.perform(post(postUrl)
                .content(json(validSymbol))
                .contentType(jsonContentType))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.text", containsString("Something strange happened")))
    }

    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var webAppContext: WebApplicationContext

    @Autowired
    @Qualifier("object2HttpJson")
    private lateinit var json: (Any) -> String

    @Value("\${telegram.bot.webhook-path}")
    private lateinit var postUrl: String

    @Before
    fun setup() {
        mockMvc = webAppContextSetup(webAppContext).build()
    }
}