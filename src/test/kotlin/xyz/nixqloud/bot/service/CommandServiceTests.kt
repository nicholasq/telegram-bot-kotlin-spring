package xyz.nixqloud.bot.service

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestContextManager
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import xyz.nixqloud.bot.TelegramApplicationTest
import xyz.nixqloud.bot.json.Chat
import xyz.nixqloud.bot.json.Message
import xyz.nixqloud.bot.json.Update
import xyz.nixqloud.bot.json.User

@RunWith(Parameterized::class)
@SpringBootTest(classes = [TelegramApplicationTest::class])
class CommandServiceTests constructor(
        val input: String,
        val expected: String
) {

    init {
        @Suppress("LeakingThis")
        TestContextManager(CommandServiceTests::class.java).prepareTestInstance(this)
    }


    companion object {

        private val commonUpdate = Update(
                updateId = 12345,
                message = Message(
                        from = User(
                                id = 12345
                        ),
                        chat = Chat(
                                id = 54321
                        )
                )
        )

        @Parameterized.Parameters
        @JvmStatic
        fun data(): Collection<Array<Any>> {
            return listOf(
                    arrayOf<Any>(
                            "/bash echo hello from bash",
                            "hello from bash"
                    ),
                    arrayOf<Any>(
                            "/python print('hello from python')",
                            "hello from python"
                    ),
                    arrayOf<Any>(
                            "/javascript console.log('hello from javascript');",
                            "hello from javascript"
                    )
            )
        }
    }

    @Test
    fun `test commands`() {

        val update = commonUpdate.apply { message!!.text = input }

        // todo disable for now.
//        mockMvc.perform(post(postUrl)
//                .content(json(update))
//                .contentType(jsonContentType))
//                .andExpect(status().isOk)
//                .andExpect(jsonPath("$.text", startsWith(expected)))

    }

    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var webAppContext: WebApplicationContext

    @Autowired
    @Qualifier("object2HttpJson")
    private lateinit var json: (Any) -> String

    @Value("\${telegram.bot.webhook-path}")
    private lateinit var postUrl: String

    @Before
    fun setup() {
        mockMvc = webAppContextSetup(webAppContext).build()
    }
}