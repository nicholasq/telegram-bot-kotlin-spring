package xyz.nixqloud.bot

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.mock.http.MockHttpOutputMessage
import org.springframework.test.web.servlet.ResultHandler
import java.nio.charset.Charset

@Configuration
class TestHelpers {

    @Bean("jackson2HttpMsgConverter")
    fun jackson2HttpMsgConverter(converters: Array<HttpMessageConverter<Any>>): MappingJackson2HttpMessageConverter {

        return converters
                .asSequence()
                .filter { it is MappingJackson2HttpMessageConverter }
                .first() as MappingJackson2HttpMessageConverter

    }

    @Bean("object2HttpJson")
    fun object2HttpJson(jackson2HttpMsgConverter: MappingJackson2HttpMessageConverter): (Any) -> String {

        return { obj ->
            with(MockHttpOutputMessage()) {
                jackson2HttpMsgConverter.write(
                        obj, MediaType.APPLICATION_JSON, this
                )
                this.bodyAsString
            }
        }
    }
}

val jsonContentType = MediaType(
        MediaType.APPLICATION_JSON.type,
        MediaType.APPLICATION_JSON.subtype,
        Charset.forName("utf8")
)

val printResponse = ResultHandler { it ->
    println(it.response.contentAsString)
}

