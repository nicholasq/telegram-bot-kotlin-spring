package xyz.nixqloud.bot

import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication()
class TelegramApplicationTest