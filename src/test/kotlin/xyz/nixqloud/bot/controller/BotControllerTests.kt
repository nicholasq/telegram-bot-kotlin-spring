package xyz.nixqloud.bot.controller

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.startsWith
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup
import org.springframework.web.context.WebApplicationContext
import xyz.nixqloud.bot.TelegramApplicationTest
import xyz.nixqloud.bot.json.Chat
import xyz.nixqloud.bot.json.Message
import xyz.nixqloud.bot.json.Update
import xyz.nixqloud.bot.jsonContentType
import xyz.nixqloud.bot.printResponse

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [TelegramApplicationTest::class])
class BotControllerTests {

    @Test
    fun `check that correct chat id is returned`() {

        val incomingUpdate = Update(
                message = Message(
                        text = "/ping",
                        chat = Chat(id = 12345)
                )
        )

        mockMvc.perform(post(postUrl)
                .content(json(incomingUpdate))
                .contentType(jsonContentType))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.chat_id", `is`(incomingUpdate.message?.chat?.id)))
                .andExpect(jsonPath("$.text", startsWith("Startup time:")))
    }

    @Test
    fun `check command list is returned`() {

        val incomingUpdate = Update(
                message = Message(
                        text = "/commands",
                        chat = Chat(id = 12345)
                )
        )

        val commandList = """/stock
                            |/ping
                            |/example
                            |/double
                            |/crypto
                            |/reddit
                            |/reminder
                            |/whatareyouthinking""".trimMargin()

        mockMvc.perform(post(postUrl)
                .content(json(incomingUpdate))
                .contentType(jsonContentType))
                .andExpect(status().isOk)
                .andDo(printResponse)
                .andExpect(jsonPath("$.chat_id", `is`(incomingUpdate.message?.chat?.id)))
                .andExpect(jsonPath("$.text", startsWith(commandList)))
    }

    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var webAppContext: WebApplicationContext

    @Autowired
    @Qualifier("object2HttpJson")
    private lateinit var json: (Any) -> String

    @Value("\${telegram.bot.webhook-path}")
    private lateinit var postUrl: String

    @Before
    fun setup() {
        mockMvc = webAppContextSetup(webAppContext).build()
    }

}
